const Task = require("../models/task.js");



module.exports.getAllTask = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({

		// name: {
		// 	firstName: requestBody.firstname,
		// 	lastName: requestBody.lastname
		// }
		name : requestBody.name
	})

	return newTask.save(). then((task, error)=>{
		if (error){
			console.log(error);
			return false; /*"Error Detected"*/
		}
		else{
		return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => 
			{

			if (err) {
				console.log(err);
				return false;
			} else {
				return true;
			}

	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId). then ((result, error) => {
		if(error){
			console.log(error)
			return false;
		}
		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if (saveErr){
				console.log(saveErr)
				return false;
			}
			else{
				return updateTask;
			}
		})
	})
}





module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}

module.exports.updateStatus = (taskId) => {
	return Task.findByIdAndUpdate(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		result.status = "complete";
		return result.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updateTask;
			}
		})
	})
}


























/*

Model.deleteMany()
Model.deleteOne()
Model.find()
Model.findById()
Model.findByIdAndDelete()
Model.findByIdAndRemove()
Model.findByIdAndUpdate()
Model.findOne()
Model.findOneAndDelete()
Model.findOneAndRemove()
Model.findOneAndReplace()
Model.findOneAndUpdate()
Model.replaceOne()
Model.updateMany()
Model.updateOne()

*/